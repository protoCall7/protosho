#
#===============================================================================
#
#         FILE: Application.pm
#
#  DESCRIPTION: protosho main application module
#
#        FILES: IP.pm, Search.pm, ExploitSearch.pm ExploitFetch.pm
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 10/12/2012 21:50:19
#     REVISION: ---
#===============================================================================

package Protosho::Application;
use Modern::Perl;
use Config::Auto;
use WWW::Shodan::API;
use base qw( CLI::Framework );

our $VERSION = '1.0';

#===  FUNCTION  ================================================================
#         NAME: command_map
#      PURPOSE: Main command name to command object mapping
#   PARAMETERS: N/A
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub command_map {
    return (
	'info'           => 'Protosho::Commands::Info',
        'ip'             => 'Protosho::Commands::IP',
	'myip'           => 'Protosho::Commands::MyIP',
	'resolve'        => 'Protosho::Commands::Resolve',
	'reverse'        => 'Protosho::Commands::Reverse',
        'search'         => 'Protosho::Commands::Search',
    );
}

#===  FUNCTION  ================================================================
#         NAME: usage_text
#      PURPOSE: Help menu usage text definition
#   PARAMETERS: N/A
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub usage_text {
    return qq{
================================================================================
protosho v1.0
Copyright (c)2012-2019 Peter Ezetta
================================================================================

		$0 <COMMAND> [--long-opt|o]

		COMMANDS
			info		- Display API Information
			ip              - Search for hosts by IP
			myip            - Display Local IP as seen from SHODAN
			resolve         - Perform DNS resolution
			reverse         - Perform reverse DNS resolution
			search          - Search SHODAN Database by Keyword

};
}

#===  FUNCTION  ================================================================
#         NAME: init
#      PURPOSE: Initialization routine.  Instantiates and caches an api object.
#   PARAMETERS: N/A
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub init {
    my ( $self, $opts ) = @_;

    my $config         = Config::Auto::parse('protosho.config');
    my $SHODAN_API_KEY = $config->{'api_key'};
    my $api            = WWW::Shodan::API->new($SHODAN_API_KEY);

    $self->cache->set( api => $api );

    return;
}

1;
