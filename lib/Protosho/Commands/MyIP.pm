#
#===============================================================================
#
#         FILE: MyIP.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 10/12/2012 00:24:42
#     REVISION: ---
#===============================================================================

package Protosho::Commands::MyIP;
use Modern::Perl;
use base qw( CLI::Framework::Command );
use Data::Printer colored => 1;

our $VERSION = '1.0';

#===  FUNCTION  ================================================================
#         NAME: run
#      PURPOSE: Main run loop of MyIP object.
#   PARAMETERS: ????
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub run {
    my ( $self, $opts, @args ) = @_;

    my $api = $self->cache->get('api');

    my $result = $api->my_ip();

    printf("Current IP:\t%s\n", $result);

    return;
}

1;
