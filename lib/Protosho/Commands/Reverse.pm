#
#===============================================================================
#
#         FILE: Reverse.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 03/16/19
#     REVISION: ---
#===============================================================================

package Protosho::Commands::Reverse;
use Modern::Perl;
use base qw( CLI::Framework::Command );
use Data::Format::Pretty::Console qw(format_pretty);

our $VERSION = '1.0';

#===  FUNCTION  ================================================================
#         NAME: run
#      PURPOSE: Main run loop of Reverse object.
#   PARAMETERS: ????
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub run {
    my ( $self, $opts, @args ) = @_;

    my $api = $self->cache->get('api');
    my $result = $api->reverse_dns($opts->{'ip'});

    print format_pretty($result);

    return;
}

sub option_spec {
    return (
        [ "ip|i=s@" => 'IPs to reverse resolve' ],
    );
}

sub validate {
    my ( $self, $opts, @args ) = @_;
    die "At least one IP is required. Usage:\n" . $self->usage() . "\n" unless $opts->{'ip'};
    return;
}

1;
