#
#===============================================================================
#
#         FILE: Resolve.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 03/16/19
#     REVISION: ---
#===============================================================================

package Protosho::Commands::Resolve;
use Modern::Perl;
use base qw( CLI::Framework::Command );
use Data::Format::Pretty::Console qw(format_pretty);

our $VERSION = '1.0';

#===  FUNCTION  ================================================================
#         NAME: run
#      PURPOSE: Main run loop of Resolve object.
#   PARAMETERS: ????
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub run {
    my ( $self, $opts, @args ) = @_;

    my $api = $self->cache->get('api');
    my $result = $api->resolve_dns($opts->{'domain'});

    print format_pretty($result);

    return;
}

sub option_spec {
    return (
        [ "domain|d=s@" => 'Domains to resolve' ],
    );
}

sub validate {
    my ( $self, $opts, @args ) = @_;
    die "At least one domain is required. Usage:\n" . $self->usage() . "\n" unless $opts->{'domain'};
    return;
}

1;
