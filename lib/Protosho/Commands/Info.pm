#
#===============================================================================
#
#         FILE: Info.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 10/12/2012 00:24:42
#     REVISION: ---
#===============================================================================

package Protosho::Commands::Info;
use Modern::Perl;
use base qw( CLI::Framework::Command );
use Data::Format::Pretty::Console qw(format_pretty);

our $VERSION = '1.0';

#===  FUNCTION  ================================================================
#         NAME: run
#      PURPOSE: Main run loop of API Info object.
#   PARAMETERS: ????
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub run {
    my ( $self, $opts, @args ) = @_;

    my $api = $self->cache->get('api');

    my $result = $api->api_info();

    # Check for errors
    if ( $result->{'error'} ) {
        print "Error: " . $result->{'error'} . "\n";
    }
    else {
	    print format_pretty($result);
    }

    return;
}

sub usage_text {
    return q{
    info: Display API info
};
}

1;
