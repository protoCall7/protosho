#
#===============================================================================
#
#         FILE: Search.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 10/12/2012 00:24:42
#     REVISION: ---
#===============================================================================

package Protosho::Commands::Search;
use Modern::Perl;
use base qw( CLI::Framework::Command );
use Data::Printer colored => 1;

sub run {
    my ( $self, $opts, @args ) = @_;

    my $api   = $self->cache->get('api');
    my $query = {};
    my $facets = [];

    $query->{'after'}      = $opts->{'after'}    if $opts->{'after'};
    $query->{'asn'}        = $opts->{'asn'}      if $opts->{'asn'};
    $query->{'before'}     = $opts->{'before'}   if $opts->{'before'};
    $query->{'city'}       = $opts->{'city'}     if $opts->{'city'};
    $query->{'country'}    = $opts->{'country'}  if $opts->{'country'};
    $query->{'ipv6'}       = 1                   if $opts->{'ipv6'};
    $query->{'screenshot'} = 1                   if $opts->{'screenshot'};
    $query->{'hostname'}   = $opts->{'hostname'} if $opts->{'hostname'};
    $query->{'isp'}        = $opts->{'isp'}      if $opts->{'isp'};
    $query->{'link'}       = $opts->{'link'}     if $opts->{'link'};
    $query->{'net'}        = $opts->{'net'}      if $opts->{'net'};
    $query->{'org'}        = $opts->{'org'}      if $opts->{'org'};
    $query->{'os'}         = $opts->{'os'}       if $opts->{'os'};
    $query->{'port'}       = $opts->{'port'}     if $opts->{'port'};
    $query->{'postal'}     = $opts->{'postal'}   if $opts->{'postal'};
    $query->{'product'}    = $opts->{'product'}  if $opts->{'product'};
    $query->{'state'}      = $opts->{'state'}    if $opts->{'state'};
    $query->{'version'}    = $opts->{'version'}  if $opts->{'version'};

    my $result = $api->search($query, $facets);

    # Check for errors
    if ( $result->{'error'} ) {
        print "Error: " . $result->{'error'} . "\n";
    }
    else {
        p $result;
    }
    return;
}

sub option_spec {
    return (
        [
            "after|a=s" =>
"Only show results that were collected after the given date (dd/mm/yyyy)."
        ],
        [
            "asn|A=s" =>
"The Autonomous System Number that identifies the network the device is on."
        ],
        [
            "before|b=s" =>
"Only show results that were collected before the given date (dd/mm/yyyy)."
        ],
        [ "city|c=s" => "Show results that are located in the given city." ],
        [
            "country|C=s" =>
              "Show results that are located within the given country."
        ],
        [ "ipv6" => "Only show results that were discovered on IPv6." ],
        [
            "screenshot" =>
              "Only show results that have a screenshot available."
        ],
        [
            "hostname=s" =>
              "Search for hosts that contain the given value in their hostname."
        ],
        [
            "isp|i=s" =>
              "Find devices based on the upstream owner of the IP netblock."
        ],
        [
            "link|l=s" =>
              "Find devices depending on their connection to the internet."
        ],
        [
            "net|n=s" =>
              "Search by netblock using CIDR notation; ex: net:69.84.207.0/24"
        ],
        [ "org|o=s" => "Find devices based on the owner of the IP netblock." ],
        [
            "os|O=s" =>
              "Filter results based on the operating system of the device."
        ],
        [
            "port|p=i" =>
"Find devices based on the services/ports that are publicly exposed on the internet."
        ],
        [ "postal|P=i" => "Search by postal code." ],
        [
            "product=s" =>
              "Filter using the name of the software/product; ex: product:Apple"
        ],
        [
            "state|s=s" =>
"Search for devices based on the state/region they are located in."
        ],
        [
            "version=s" =>
"Filter the results to include only products of the given version; ex product:apache version:1.3.37"
        ],
    );
}

sub validate {
    my ($self, $opts, @args) = @_;

    die "No filters given. Usage:" . $self->usage_text() . "\n" unless @args;
}

1;
