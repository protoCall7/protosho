#
#===============================================================================
#
#         FILE: IP.pm
#
#  DESCRIPTION:
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Peter H. Ezetta (PE), peter.ezetta@zonarsystems.com
# ORGANIZATION:
#      VERSION: 1.0
#      CREATED: 10/12/2012 00:24:42
#     REVISION: ---
#===============================================================================

package Protosho::Commands::IP;
use Modern::Perl;
use base qw( CLI::Framework::Command );
use Data::Format::Pretty::Console qw(format_pretty);

our $VERSION = '1.0';

#===  FUNCTION  ================================================================
#         NAME: run
#      PURPOSE: Main run loop of IP object.
#   PARAMETERS: ????
#      RETURNS: ????
#  DESCRIPTION: ????
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub run {
    my ( $self, $opts, @args ) = @_;

    my $api = $self->cache->get('api');

    my $search = { IP => $opts->{'ip'} };

    $search->{'HISTORY'} = 1 if $opts->{'history'};
    $search->{'MINIFY'}  = 1 if $opts->{'minify'};

    my $result = $api->host_ip($search);

    # Check for errors
    if ( $result->{'error'} ) {
        print "Error: " . $result->{'error'} . "\n";
    }
    else {
	print format_pretty($result);
    }

    return;
}

sub option_spec {
    return (
        [ 'ip|i=s'    => 'IP address to search' ],
        [ 'history|h' => 'Show historical banners' ],
        [
            'minify|m' =>
              'Only return general port and host information, no banners'
        ],
    );
}

sub usage_text {
    return q{
    ip --ip|i=<IP Address> [--history|h] [--minify|m]:

    OPTIONS:
        --ip:
	--history: All historical banners should be returned
	--minify:  Return the list of ports and general host information, no banners.
};
}

sub validate {
    my ( $self, $opts, @args ) = @_;
    die "IP address required. Usage:\n" . $self->usage() . "\n" unless $opts->{'ip'};
    return;
}

1;
